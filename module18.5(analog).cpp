﻿// module18.5(analog).cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
class Stack {
private:
    int n = 20;
    int* arr = new int[n];
    
    int stackSize;
public:
    Stack() {
        
        stackSize = 0;
    }
    ~Stack() {
        delete[] arr;
    }
    void push(int number){
        if (stackSize < n) {
            arr[stackSize++] = number;
        }
        else {
            std::cout << "Cтек полон\n";
        }
    }
    bool isEmpty() {
        return stackSize == 0;
    }
    void top() {
        if (stackSize > 0) {
           std::cout << "Верхний элемент - " << arr[stackSize - 1] << "\n";// верхний элемент
        }
        else {
            std::cout << "Стек пуст\n";
            
        }
        
    }
    int pop() {
        if (stackSize > 0) {
             
             int delNumber = --stackSize;
             return delNumber;
            
        }
        else {
            std::cout << "Стек пуст\n";
            return 0;
        }
    }
    void show() {
        if (stackSize > 0) {
            for (int i = stackSize - 1; i >= 0; --i) {
                std::cout << arr[i];
            }
            std::cout << "\n";
        }
        else {
            std::cout << "Стек пуст\n";
        }
    }
    
}; 

int main()
{
    setlocale(LC_ALL, "rus");
    
    Stack stack;

    std::cout << (stack.isEmpty() ? "Cтек пуст":"Стек полон")<<"\n";
    stack.push(5);
    stack.top();
    stack.push(2);
    stack.push(3);
    stack.push(4);
    stack.show();
    stack.top();
    stack.pop();
    stack.top();
    
    return 0;
    
    
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
